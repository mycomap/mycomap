import '../types.js'
import debug from 'debug'
import BeeHive from './bee-hive.js'
import { generateID, hex, shortHex, sortBy } from '../utils.js'

const log = debug('mycomap:api:models:guide')
const { assign } = Object

export default class Guide extends BeeHive {
  /**
   * @param {HyperClient} client - an object containing corestore and networker
   * @param {string=} [pid = null] - the primary ID of the guide
   * @param {object} opts
   */
  constructor (client, pid, localdb, opts = {}) {
    super(client, pid, opts)
  }

  /**
   * build state of latest guide metadata from all guide feeds
   * pid and createdAt are always from the original message
   * @returns {Metadata}
   */
  async metadata () {
    const stream = this.createReadStream({ gt: 'metadata/', lt: 'metadata0' })
    const entries = []
    // reduce all metadata records in all dbees
    for await (const entry of stream) {
      let updatedAt = Number(entry.key.split('/')[1])
      entries.push({ ...entry.value, updatedAt })
    }
    // sort by updatedAt field
    const sorted = entries.sort(sortBy('updatedAt'))
    let first = sorted[0] || { createdAt: Date.now() }
    // compile time sorted into a single record
    let result = {}
    for (const entry of sorted) {
      result = { ...result, ...entry }
    }
    // merge in pid, first createdAt and peers
    return {
      ...result,
      pid: this.pid,
      createdAt: Number(first.createdAt),
      createdBy: {},
      peers: this.peers
    }
  }

  /**
   * add spopt to this guide given an object with the spot parameters,
   * after generating and assigning a spot ID
   * @returns {Spot}
   */
  async addSpot (spotParams) {
    const spotId = generateID()
    const params = assign({ id: spotId }, spotParams)
    await this.db.put(`spot/${spotId}`, params)
    // we fetch the spot again because we need the sequence number to index it
    // TODO: speak to hyperdivision about returning this from a put
    const spot = await this.db.get(`spot/${spotId}`)
    return spot
  }
}
