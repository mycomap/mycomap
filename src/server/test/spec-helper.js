import tmp from 'tmp'
import { Readable } from "stream"
import { mkdir, rm } from 'fs/promises'

tmp.setGracefulCleanup()

/**
 * sleep a few milliseconds
 * @param {number} timeout
 */
export function sleep (timeout) {
  return new Promise((res) => {
    setTimeout(res, timeout)
  })
}

/*
 * create a temporary storage directory
 * e.g. /tmp/mycomap-6372j2UVIQfqm04N
 *
 * @returns {string} - a path to a tmp directory
 */
export function tmpdir () {
  return new Promise((resolve, reject) => {
    tmp.dir({ prefix: 'mycomap', keep: false }, (err, path) => {
      if (err) return reject(err)
      return resolve(path)
    })
  })
}

/*
 * remove temporary storage after a test
 */
export async function cleanup (storage) {
  return await rm(storage, { recursive: true, force: true })
}

/*
 * a mock express request object
 * @returns {Object}
 */
export function mockRequest (body = {}) {
  const req = {}
  req.body = body
  return req
}

/*
 * a mock express response object
 * @returns {Object}
 */
export function mockResponse (res = {}) {
  res.status = jest.fn().mockReturnValue(res.status)
  res.json = jest.fn().mockReturnValue(res.json)
  res.sendStatus = jest.fn().mockReturnValue(res.sendStatus)
  return res
}

/*
 * a mock that returns a readable stream
 * @param {Array} data - a list of messages to stream
 * @param {Object} opts - readable stream opts
 */
export function mockReadStream (data) {
  return jest.fn().mockImplementation((opts) => {
    const readable = new Readable({ objectMode: true, ...opts })
    readable.finish = readable.end = readable.close
    for (const item of data) {
      readable.push(item)
    }
    readable.push(null)
    return readable
  })
res.sendStatus}
