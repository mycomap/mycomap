import path from 'path'
import express from 'express'
import debug from 'debug'
import Corestore from 'corestore'
import CorestoreNetworker from '@corestore/networker'
import Hyperbee from 'hyperbee'
import fs from 'fs/promises'
import level from 'level'

import HyperClient from './lib/hyper-client.js'
import routes from './routes.js'
import logRequest from './middleware/log-request.js'
import * as constants from './constants.js'
import { hex } from './utils.js'

import GuidesController from './controllers/guides.js'
import ProfileController from './controllers/profile.js'

import GuidesValidator from './validators/guides.js'
import ProfileValidator from './validators/profile.js'

const log = debug('mycomap:server')
const { assign, values } = Object

export default class Server {
  /**
   * instantiate a mycomap app
   *
   * @constructor
   * @param {Object} opts
   * @param {string} opts.storage - location of a corestore
   * @param {string|number} opts.port - the http server's port
   * @param {string} opts.namespace - corestore namespace
   */
  constructor (opts = {}) {
    this.storage = opts.storage || constants.DEFAULT_STORAGE
    this.port = opts.port || constants.DEFAULT_PORT
    this.corestoreNamespace = opts.namespace || constants.CORESTORE_NAMESPACE
    this.app = express()
    this.app.use(express.json())
    this.app.use(express.urlencoded({ extended: true }))
    this.app.use(logRequest)
  }

  /**
   * initialize the app, setup hyper* modules (corestore and networker)
   * generate and start controllers and validators setup http,
   * routes, and create a local index database using leveldb
   */
  async setup () {
    this.client = new HyperClient(path.join(this.storage, 'store'))
    await this.client.ready()
    // pop open our localdb
    this.localdb = new Hyperbee(this.client.corestore.default(), constants.defaultEncoding)
    // make it ready, and ensure storage exists
    await Promise.all([
      this.localdb.ready(),
      fs.mkdir(this.storage, { recursive: true })
    ])
    // setup our index database
    this.indexdb = level(path.join(this.storage, 'index.db'))
    // initialize all controllers
    this.controllers = {
      profile: new ProfileController(this.client, this.localdb),
      guides: new GuidesController(this.client, this.localdb, this.indexdb)
    }
    // and validators
    this.validators = {
      profile: ProfileValidator(),
      guides: GuidesValidator()
    }
    // generate routes
    this.app.use('/', routes(this.controllers, this.validators))
  }

  async start () {
    // load all controllers
    await Promise.all(values(this.controllers).map((controller) => controller.start()))
    // fire up the web server
    this.httpServer = await this.app.listen(this.port)
  }

  /**
   * close all open feeds then shutdown hyper* and http servers
   */
  async stop () {
    // close all controllers
    await Promise.all(values(this.controllers).map((controller) => controller.stop()))
    // close our hyperclient
    await Promise.all([
      this.indexdb.close(),
      this.client.close()
    ])
    // close the web server if it exists
    if (this.httpServer) {
      await this.httpServer.close()
    }
  }
}
