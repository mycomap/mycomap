import debug from 'debug'
import path from 'path'
import { fileURLToPath } from 'url'
import fs from 'fs/promises'
import Server from './server.js'
import { colourize, hex, prettyHash } from './utils.js'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const log = debug('mycomap:main')

/*
 * start the mycomap web server
 * @param {Object} opts - an options object
 * @param {string} opts.storage - the storage location of your corestore and other app data
 * @param {string|integer} opts.port - the http server port
 */
async function main (opts = {}) {
  const server = new Server(opts)
  await server.setup()

  const mushroom = await fs.readFile(path.join(__dirname, '..', '..', 'assets', 'mushroom.ascii'))
  const banner = await fs.readFile(path.join(__dirname, '..', '..', 'assets', 'banner.ascii'))

  console.log(mushroom.toString())
  console.log(banner.toString())

  const colour = colourize(223)
  console.log(`\nstorage at ${colour(`file://${path.resolve(server.storage)}`)}`)
  console.log(`localdb address is ${colour(`hyper://${prettyHash(server.localdb.feed.key)}`)}`)
  console.log(`listening on ${colour(`http://localhost:${server.port}`)}`)
  console.log(`open api documentation at ${colour(`http://localhost:${server.port}/api-docs`)}\n\n`)

  process.on('uncaughtException', async (err) => {
    log({ msg: '[FATAL]', error: err })
  })

  await server.start()

  process.on('SIGINT', async (code) => {
    log({ msg: '[SIGINT]', payload: code })
    await server.stop()
  })
}

/* poinch */
main({
  storage: process.env.STORAGE,
  port: process.env.PORT
})
