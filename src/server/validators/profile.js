import { body } from 'express-validator'

export default () => ({
  put: [
    body('nickname')
      .isString()
      .bail()
  ]
})
