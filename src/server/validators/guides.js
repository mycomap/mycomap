import { body } from 'express-validator'

export default () => ({
  addSpot: [
    body('lat')
      .isFloat()
      .withMessage('invalid: must be a number')
      .bail()
      .custom((lat) => ((lat <= 90) && (lat >= -90)))
      .withMessage('invalid: must be between -90 and 90')
      .bail(),

    body('lng')
      .isFloat()
      .withMessage('invalid: must be a number')
      .bail()
      .custom((lng) => ((lng <= 180) && (lng >= -180)))
      .withMessage('invalid: must be between -180 and 180')
      .bail(),

    body('name')
      .isString()
      .withMessage('invalid: must be a string')
      .bail(),

    body('description')
      .isString()
      .withMessage('invalid: must be a string')
      .bail(),

    body('tags')
      .isArray()
      .withMessage('invalid: must be an array of strings')
      .bail()
  ]
})
