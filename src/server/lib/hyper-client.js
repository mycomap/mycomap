import '../types.js'
import Corestore from 'corestore'
import Networker from '@corestore/networker'
import RAM from 'random-access-memory'

export default class HyperClient {
  #corestore = null
  #networker = null
  #storage = null
  #bootstrap = null
  /**
   * @param {string|function} storage - a storage path or random access function
   * @param {Object} opts
   * @param {number} opts.bootstrap - an optional port to reuse
   */
  constructor (storage, opts) {
    this.#storage = (storage && typeof storage === 'object') ? RAM : storage || RAM
    this.#corestore = new Corestore(this.storage)
    this.#networker = new Networker(this.corestore, opts)
  }

  /**
   * setup corestore and networker
   */
  async ready () {
    await Promise.all([
      this.corestore.ready(),
      this.networker.listen()
    ])
  }

  /**
   * close corestore and networker
   */
  async close () {
    await Promise.all([
      this.corestore.close(),
      this.networker.close()
    ])
  }

  /**
   * corestore storage location / function
   */
  get storage () {
    return this.#storage
  }

  /**
   * the corestore instance
   */
  get corestore () {
    return this.#corestore
  }

  /**
   * the networker instance
   */
  get networker () {
    return this.#networker
  }
}
