import Home from './components/views/Home.vue'
import Guides from './components/views/Guides.vue'
import NewGuide from './components/views/NewGuide.vue'
import Profile from './components/views/Profile.vue'
import Settings from './components/views/Settings.vue'

export default [
  { path: '/', name: 'home', component: Home },
  { path: '/guide/:pid', name:'guide', component: Home, props: true },
  { path: '/guides/new', name:'new-guide', component: NewGuide },
  { path: '/guides', name: 'guides', component: Guides },
  { path: '/profile', name: 'profile', component: Profile },
  { path: '/settings', name: 'settings', component: Settings }
]
