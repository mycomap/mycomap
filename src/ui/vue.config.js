module.exports = {
  outputDir: '../../dist',
  assetsDir: './assets',
  indexPath: './assets/index.html',
  pages: {
    index: {
      entry: 'main.ts',
      template: '../../assets/index.html'
    }
  },
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:5000',
        ws: true,
        changeOrigin: true
      }
    },
    overlay: {
      warnings: false,
      errors: false
    }
  }
}
