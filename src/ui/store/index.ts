import profile from './profile'
import guides from './guides'

export default {
  modules: {
    profile,
    guides
  }
}
