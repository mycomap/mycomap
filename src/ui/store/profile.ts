import axios from 'axios'
import { Module, GetterTree, MutationTree, ActionTree } from 'vuex'

import { RootState, ProfileState, ProfileModel } from './types'

const state: ProfileState = {
  profile: null
}

const mutations: MutationTree<ProfileState> = {
  setProfile(state, profile: ProfileModel) {
    state.profile = profile 
  }
}

const actions: ActionTree<ProfileState, RootState> = {
  /**
   * GET /api/profile
   */
  fetchProfile({ commit }) : void {
    axios({
      url: '/api/profile'
    }).then((response) => {
      commit('setProfile', response.data)
    }, (error) => {
      console.log(error)
    })
  }
}

const getters: GetterTree<ProfileState, RootState> = {
  getProfile(state) : ProfileModel | null {
    return state.profile
  }
}

export default {
  namespaced: true as true,
  state,
  mutations,
  actions,
  getters
}
