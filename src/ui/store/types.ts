export interface RootState {}

export interface ProfileModel {
  nickname: string
  color?: string
}

export interface ProfileState {
  profile: ProfileModel | null
}

export interface GuideModel {
  id: string
  name: string
  description: string
  createdAt: number
  createdBy: ProfileModel | null
  updatedAt: number
  peeds: number
}

export interface SpotModel {
  lat: number
  lng: number
  name: string
  description: string
  tags: Array<string>
}

export interface GuidesState {
  guides: Array<GuideModel>
  spots: Array<SpotModel>
}
