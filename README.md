# mycomap

## Table of Contents

<!-- vim-markdown-toc GFM -->

- [About](#about)
- [Installing](#installing)
- [Contributing](#contributing)
- [Credits](#credits)
- [License](#license)

<!-- vim-markdown-toc -->

## About

Mycomap is a map for recording and documenting discoveries and encounters with fungi. the map allows you to record 'spots' in specific locations, to which you can add metadata such as common names, species identification, photographs, and your own tags. you can selectively share your spots with your peers by creating 'guides', circles of trust where friends for shared secret mushroom spots.

Mycomap is built on the hypercore-protocol using corestore, hyperswarm, hyperbee and leveldb, with an express json api and a vuejs user interface and leaflet with open street maps.

Each peer has a primary local hypercore for their spots, as well as references to guides. Each guide has a unique **pid** (primary id) to group hypercores. This is used by all peers, and is the first 8 characters of the **address** or hypercore feed key for the creators hypercore. Data points recording lat and lng are small, and cheap, so granular sharing between peers simply duplicates spots in different hypercores.

## Installing

Mycomap is still under early development. if you want to take a peek, first install node v16 using a version manager such as [`nvm`](https://github.com/nvm-sh/nvm) or [`n`](https://www.npmjs.com/package/n). then setup the project and start the development servers....

```bash
# clone the repo
git clone git@gitlab.com:mycomap/mycomap.git && cd mycomap
# install dependencies
npm install
# start the api server
cd src/server && npm run dev
# in a new terminal, start the ui dev server
cd ../ui && npm run serve
```

To run two separate api servers in development, setup separate corestores, then choose a different port for the api web server:

```bash
cd src/server
# in one terminal
STORAGE=store_1 PORT=5000 npm run dev
# then another
STORAGE=store_2 PORT=5001 npm run dev
```

## Contributing

- Please get in touch directly with us by creating an issue if you want to get involved
- See our [development board](https://gitlab.com/mycomap/mycomap/-/boards)

## Credits

- Text generated using [`toilet-0.2`](http://caca.zoy.org/wiki/toilet)
- Mushroom ASCII art by Stephen CES Wilson (from https://www.asciiart.eu/plants/mushroom)

## License

[`AGPL-3.0-or-later`](./LICENSE)
